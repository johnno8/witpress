package controllers;

import java.util.*;

import play.*;
import play.mvc.*;

import models.*;

public class PublicBlogDirectory extends Controller
{
  public static void index()
  {
    ArrayList<User> usersWithBlogs = new ArrayList<User>();
    
    List<User> users = User.findAll();
    List<Post> posts = Post.findAll();
    for(User user : users)
    {
      for(Post post : posts)
      {
        if(post.madeBy == user)
        {
          usersWithBlogs.add(user);
          break;
        }
      }
    }
    render(usersWithBlogs);
  }
}
