package controllers;

import java.util.*;

import play.*;
import play.mvc.*;

import models.*;

public class BlogPost extends Controller
{

  public static void addComment(Long id, String content)
  {
    Post post = Post.findById(id);
    Comment comment = new Comment(content);
    post.addComment(comment);
    post.save();
    Blog.viewPost(id);
  }
  
  public static void deleteComment(Long postId, Long commentId)
  {
    Comment comment = Comment.findById(commentId);
    Post post = Post.findById(postId);
    post.comments.remove(comment);
    post.save();
    Blog.viewPost(postId);
  }
}
