package controllers;

import play.*;
import play.mvc.*;

import models.*;

public class Accounts extends Controller
{
  public static void login()
  {
    render();
  }
  
  public static void signup()
  {
    render();
  }
  
  public static void register(User user)
  {
	user.save();
	login();
  }
  
  public static void logout()
  {
    session.clear();
    login();
  }
  
  public static void authenticate(String email, String password)
  {
	Logger.info("Attempting to authenticate with " + email + " : " + password);
	User user = User.findByEmail(email);
    if((user != null) && (user.checkPassword(password) == true))
	{
	  Logger.info("Authentication successful");
	  session.put("logged_in_userid", user.id);
	  Blog.create();
	}
	else
	{
	  Logger.info("Authentication failed");
	  login();
	}
  }
  
  public static User getLoggedInUser()
  {
    User user = null;
    if (session.contains("logged_in_userid"))
    {
      String userId = session.get("logged_in_userid");
      user = User.findById(Long.parseLong(userId));
    }
    else
    {
      login(); 
    }
    return user;
  }
}
