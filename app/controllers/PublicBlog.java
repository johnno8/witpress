package controllers;

import java.util.*;

import play.*;
import play.mvc.*;

import models.*;

public class PublicBlog extends Controller
{
  public static void show(Long id)
  {
    User user = User.findById(id);
    List<Post> reversePosts  = new ArrayList();
    List<Post> posts = Post.findAll();
    for(Post post : posts)
    {
      if(post.madeBy == user)
      {
        reversePosts.add(post);
      }
    }
    
    Collections.reverse(reversePosts);

    User loggedInUser = null;
	if (session.contains("logged_in_userid"))
	{
      String userId = session.get("logged_in_userid");
      loggedInUser = User.findById(Long.parseLong(userId));
    }
    render(user, loggedInUser, reversePosts); 
  }
  
  public static void newComment(Long postid, Long userID, Long loggedInUserID, String content)
  {    
    Post post = Post.findById(postid);
    Comment comment = new Comment(content);

    post.addComment(comment);
    post.save();
    show(userID);
  } 
}
