package controllers;

import java.util.*;

import play.*;
import play.mvc.*;

import models.*;

public class Blog extends Controller
{
  public static void create()
  {
    String userId = session.get("logged_in_userid");
    User madeBy = User.findById(Long.parseLong(userId));
    
    List<Post> posts = Post.find("byMadeBy", madeBy).fetch();
    
    render(posts);
  }
  
  public static void addPost(String title, String content)
  {
    String userId = session.get("logged_in_userid");
    User madeBy = User.findById(Long.parseLong(userId));
    
    Logger.info("Post by " + madeBy.firstName + " " + madeBy.lastName + ": " + content);
    
    madeBy.addPost(title, content);
    create();
  }
  
  public static void viewPost(Long id)
  {
    Post post = Post.findById(id);
    render(post);
  }
  
  public static void deletePost(Long id)
  { 
    Post post = Post.findById(id);
    
    post.delete();
    create();
  }
}
