package models;

import javax.persistence.Entity;
import play.db.jpa.Model;
import javax.persistence.*;
import java.util.List;
import java.util.ArrayList;

@Entity
public class User extends Model
{
  public String firstName;
  public String lastName;
  public String email;
  public String password;
  
  /*@OneToMany(cascade=CascadeType.ALL)
  public List<Post> posts = new ArrayList<Post>();*/
	  
  public User(String firstName, String lastName, String email, String password)
  {
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.password = password;
  }
  
  public static User findByEmail(String email)
  {
    return find("email", email).first();
  }
  
  public boolean checkPassword(String password)
  {
    return this.password.equals(password);
  }
  
  
  public void addPost(String title, String content)
  {
    Post post = new Post(this, title, content);
    //posts.add(post);
    post.save();
  }
}
