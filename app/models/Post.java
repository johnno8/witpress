package models;

import javax.persistence.*;
import play.db.jpa.*;
import java.util.*;

@Entity
public class Post extends Model
{
  
  public String title;
  @Lob
  public String content;
 
  @ManyToOne
  public User madeBy;
  
  @OneToMany(cascade=CascadeType.ALL)
  public List<Comment> comments;
  
  public Post(User madeBy, String title, String content)
  {
    this.madeBy = madeBy;
    this.title = title;
    this.content = content;
  }
  
  public void addComment(Comment comment)
  {
    comments.add(comment);
  }
  
}
